from chevallib.algebra import *


def legacy_check():
    from legacy.Chevalley2 import Chevalley
    E = Chevalley()
    # input data according to the tips on the screen
    # 3  # The dimension of the vector space
    # 0  # Tag (auxiliary)
    # 1  # Verbosity
    print(E.x(1), E.w(1), E.Q(1))  # Will give x, w and Q elements
    print("Q1 Q3 == Q3 Q1 :   ", E.check_Q_commuting())  # Checks whether Q(1) and Q(3) commute. Must be True.
    print("Q1 ** 3 == E :     ", E.check_Q_cubed(1))  # Checks whether Q(i) ^ 3 == I. Must be true.


if __name__ == "__main__":
    # c3 = rs('C3')
    # pp(c3.basis)

    # set_default_root_system(RootSystem('C4'))
    rsp = DefaultRS
    print(rsp.rs)

    al = Algebra()
    print(al.x1)

    legacy_check()
