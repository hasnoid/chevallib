from abc import ABC, abstractmethod
from typing import List, Dict, Tuple, Iterable, Union, Set, Optional
from pandas import DataFrame

import numpy as np
from numpy import array, zeros, dot
from sympy import Matrix, Rational


def singleton_rs(class_):
    # root_systems: Dict[Tuple[str, int], RootSystem] = {}  todo: rewrite
    root_systems = dict()

    def getinstance(*args, **kwargs):
        class_key = (class_.__name__, *args, *kwargs)
        if class_key not in root_systems:
            root_systems[class_key] = class_(*args, *kwargs)
        return root_systems[class_key]

    return getinstance


def cartan(a: array, b: array) -> int:
    """
    Returns a cartan number for an ordered pair of roots. \n
    <a, b> = 2 (a, b) / (b, b)
    """

    # todo: since a, b can be lists of sympy.Rational, this condition must be modified (extended)
    #  e.g. && denom.is_integer
    num, denom = 2 * dot(a, b), dot(b, b)
    if num % denom != 0:
        raise RuntimeError(f"Runtime error while computing cartan product of a = {a}, b = {b}. "
                           f"Computed value is not integer as it is expected to be.")
    return num // denom


def reflect(vec: array, plain_norm: array) -> array:
    """
    :param vec: what root must be reflected
    :param plain_norm: a normal root to a reflecting plane
    :return: reflected vector
    """
    return vec - plain_norm * cartan(vec, plain_norm)


class RootSystem(ABC):
    @property
    @abstractmethod
    def name(self) -> str:
        pass

    @property
    @abstractmethod
    def n(self) -> int:
        pass

    @property
    @abstractmethod
    def size(self) -> int:
        pass

    @property
    @abstractmethod
    def basis(self) -> Set[Tuple[int, ...]]:
        pass

    @property
    @abstractmethod
    def roots(self) -> List[array]:
        pass

    @property
    @abstractmethod
    def root_to_index(self) -> Dict[Tuple[int, ...], int]:
        pass

    @property
    @abstractmethod  # .             todo: looks weird...
    def structure_constants(self) -> List[List[Tuple[Tuple[Optional[int], Optional[int]], ...]]]:
        pass

    def __init__(self) -> None:
        self._n: int = 0
        self._name: str = ''
        self._size: int = 0
        self._basis: Tuple[int, ...] = ()
        self._roots: List[array] = []
        self._root_to_index: Dict[Tuple[int, ...], int] = {}
        self._root_to_order: Dict[Tuple[int, ...], int] = {}
        self._structure_constants: List[List[Tuple[Tuple[Optional[int], Optional[int]], ...]]] = []
        self._N_ab = []

    def is_basic(self, vec: Iterable[int]) -> bool:
        """
        Tells whether a given vector belongs to the basis of the current system.
        :param vec: vector to be tested
        """
        return tuple(vec) in self.basis

    def is_root(self, vec: Iterable[int]) -> bool:
        """
        Tells whether a given vector is a root of the current system.
        :param vec: vector to be tested
        """
        return tuple(vec) in self.root_to_index

    @property
    def num_positive(self):
        return self.size // 2

    def is_positive(self, vec: Iterable[int]) -> bool:
        """
        Tells whether a given vector is a positive root.
        :param vec: vector to be tested
        """
        if not self.is_root(vec):
            raise ValueError("Passed vector is not a root. Only roots can be tested.")
        i = self.get_index(vec)
        return i < self.num_positive

    def is_negative(self, vec: Iterable[int]) -> bool:
        """
        Tells whether a given vector is a negative root.
        :param vec: vector to be tested
        """
        return not self.is_positive(vec)

    def get_index(self, vec: Iterable[int]) -> int:
        """
        :param vec: vector to be tested
        :return: index of a given vector in the fixed ordering inside current root system
        """
        if not self.is_root(vec):
            raise ValueError("Passed vector is not a root. Only roots can be tested.")
        return self.root_to_index[tuple(vec)]

    def get_negative_index(self, w: Union[int, Iterable[int]]) -> int:
        if isinstance(w, int):
            vec = -self.roots[w]
        else:
            vec = tuple(-_ for _ in w)
        return self.get_index(vec)

    # todo: optimize, e.g. precompute matrix of transformation or even all decompositions
    def decompose(self, root):
        decomposed_root = Matrix(self.roots[:self.n]).T.inv() * Matrix(root)
        res = []
        for i in range(len(decomposed_root)):
            if not decomposed_root[i] == 0:
                res.append((decomposed_root[i], i))
        return res

    @staticmethod
    def dual(root):
        return 2 * root / root.dot(root)

    # todo: same
    def decompose_dual(self, root):
        decomposed_root = Matrix([self.dual(x) for x in self.roots[:self.n]]).T.inv() * Matrix(self.dual(root))
        res = []
        for i in range(len(decomposed_root)):
            if not decomposed_root[i] == 0:
                res.append((decomposed_root[i], i))
        return res

    def structure_constants_to_csv(self, file_name=None) -> None:
        file_name = file_name if file_name is not None else f'output/{self.name}_structure_matrix.csv'
        df = DataFrame([list(map(str, _)) for _ in self._structure_constants])
        df.to_csv(file_name)

    def height(self, root):
        assert self.is_root(root)
        return sum([coeff for coeff, _ in self.decompose(root)])

    def _precompute_structure_constants_of_correspondent_lie_algebra(self) -> None:
        """
        Initializes structure_constants (typically - after RootSystem change)
        structure_constants[i][j] = (num, k), s.t.: [elem_i, elem_j] = num * elem_k

        todo: rewrite docstring using common view (not only C_n case)

        Recall the following:
        - in C_n root system:
                           positive roots                     negative roots
        ----------------------/\\------------------  ---------------/\\-----------------
             basic roots
        ---------/\\-------
        x_1  x_2  ...  x_n    x_{n+1}  ...  x_{n^2}  -x_1 ... -x_n  -x_{n+1} ... x{n^2}
         |    |         |       |             |        |       |        |           |
         0....1........n-1      n...........n**2-1   n**2..n**2+n-1  n**2+n.......n**2

        - in algebra:
        First 2 * n**2 elements - in the same order. Last n are h_i

        Now how the algebra basis acts being commuted (<a, b> := cartan(a, b))
        <meta>  [a, b] = -[b, a]
        [h_i, h_j] = 0
        [h_i, x_alpha] = <basic_root[i], alpha> x_alpha
        [x_alpha, x_{-alpha}] = \\Sum z_i h_i,  z_i is integer
        [x_a, x_b] = 0 if a + b not in roots
        [x_a, x_b] = +-(r+1) x_{a+b},  r is taken from a-series of b root: b-ar, ..., b-a, b, b+a, ..., b+qa
        """

        # todo: to a separate function
        _ = sorted([x for x in self.roots if self.is_positive(x)],
                   key=lambda r: (self.height(r), tuple(Matrix(self.roots[:self.n]).T.inv() * Matrix(r))))
        _ = [tuple(-x) for x in _[::-1]] + [tuple(x) for x in _]
        self._root_to_order = dict(zip(_, range(len(_))))
        # todo: add property `root_to_order`(or make it local since now it is used only here once)
        # todo: maybe make this mapping not root->order, but index_of_root->order

        rs_sz, n, num_pos = self._size, self._n, self.num_positive
        alg_basis_size = rs_sz + n

        self._structure_constants: List[List[Tuple[Tuple[Optional[int], Optional[int]], ...]]] = \
            [[((None, None),)] * alg_basis_size for _ in range(alg_basis_size)]

        # Handling [x, x] (== 0 for all x by the definition of [*, *])
        for i in range(rs_sz + n):
            self._structure_constants[i][i] = ((0, -1),)

        # Handling [h, h]
        for i in range(rs_sz, rs_sz + n):
            for j in range(rs_sz, rs_sz + n):
                self._structure_constants[i][j] = ((0, -1),)

        # Handling [h, x] and [x, h]
        for i in range(rs_sz):
            for j in range(rs_sz, rs_sz + n):
                num = cartan(self.roots[i], self.roots[j - rs_sz])
                self._structure_constants[j][i] = ((num, i),)  # [h, x_{alpha}]
                self._structure_constants[i][j] = ((-num, i),)  # [x_{alpha}, h]

        # Handling [x_{alpha}, x_{-alpha}]
        for i in range(num_pos):
            res_pos, res_neg = [], []
            for val, idx in self.decompose_dual(self.roots[i]):
                res_pos.append((val, rs_sz + idx))
                res_neg.append((-val, rs_sz + idx))

            self._structure_constants[i][num_pos + i] = tuple(res_pos)
            self._structure_constants[num_pos + i][i] = tuple(res_neg)

        # Handling [x_{alpha}, x_{beta}], alpha + beta != 0
        self._N_ab = [list([None] * (self.size + self.n)) for _ in range((self.size + self.n))]
        for i in range(rs_sz):
            for j in range(i + 1, rs_sz):
                _, __ = self._structure_constants[i][j][0]
                if _ is not None:
                    continue

                s = self.roots[i] + self.roots[j]

                if not self.is_root(s):
                    self._structure_constants[i][j] = ((0, -1),)
                else:
                    self._structure_constants[i][j] = \
                        ((self.N_ab(i, j), self.get_index(s)),)

                _, __ = self._structure_constants[i][j][0]
                self._structure_constants[j][i] = ((-_, __),)

                i_neg = self.get_negative_index(i)
                j_neg = self.get_negative_index(j)
                ij_res = -1 if __ == -1 else self.get_negative_index(__)
                self._structure_constants[i_neg][j_neg] = ((-_, ij_res),)
                self._structure_constants[j_neg][i_neg] = ((_, ij_res),)

        self.structure_constants_to_csv()

    def N_ab(self, index1, index2) -> int:
        if self._N_ab[index1][index2] is not None:
            return self._N_ab[index1][index2]

        # todo: does such function placement in code affect runtime? Should I put it out of N_ab?
        def sign_ab(i: int, j: int) -> int:
            """
            Note: in fact, there is a freedom degree in choosing signs for extraspecial pairs of roots,
            but we assume all of them to equal 1. Check comments inside code below for more info.
            """
            a, b = self.roots[i], self.roots[j]

            # Checking whether (a, b) is a special pair of roots
            # A pair (a, b) is special if a, b - positive and 0 < a < b.
            # Ordering is already stored by the root system.
            if self.is_positive(a) and self.is_positive(b) and \
                    self._root_to_order[tuple(a)] < self._root_to_order[tuple(b)]:
                # Finding an exstraspecial pair for this one
                a_minimal, ksi = a, a + b
                for root in self.roots:
                    if self.is_root(ksi - root) and \
                            self.is_positive(root) and \
                            self.is_positive(ksi - root):
                        if self._root_to_order[tuple(a_minimal)] > self._root_to_order[tuple(root)]:
                            a_minimal = root
                # The extraspecial pair we were searching for
                a_minimal, b_minimal = a_minimal, ksi - a_minimal

                # noinspection PyTypeChecker
                if all(a == a_minimal):
                    # the sign of a particular extraspecial
                    return 1

                def sign(num):
                    return 0 if num == 0 else (1 if num > 0 else -1)

                def norm_squared(vec):
                    return np.square(vec).sum()

                return sign_ab(self.get_index(a_minimal), self.get_index(b_minimal)) * \
                       sign(
                           self.N_ab(self.get_index(b), self.get_index(-a_minimal)) *
                           self.N_ab(self.get_index(a), self.get_index(-b_minimal)) /
                           norm_squared(b - a_minimal) +
                           self.N_ab(self.get_index(-a_minimal), self.get_index(a)) *
                           self.N_ab(self.get_index(b), self.get_index(-b_minimal)) /
                           norm_squared(a - a_minimal)
                       )
            else:  # The pair is not special
                m = 1
                if not self.is_root(a + b):
                    return 0
                if self.is_negative(b):
                    a, b, m = -a, -b, -m
                if self.is_negative(a):
                    if self._root_to_order[tuple(-a)] < self._root_to_order[tuple(b)]:
                        a, b = a + b, -a
                        m = -m
                    else:
                        a, b = b, -a - b
                if self._root_to_order[tuple(a)] > self._root_to_order[tuple(b)]:
                    a, b, m = b, a, -m
                return m * sign_ab(self.get_index(a), self.get_index(b))

        def val_ab(r1: Union[int, 'RootSystem'], r2: Union[int, 'RootSystem']) -> int:
            r = 0

            if isinstance(r1, int):
                a, b = self.roots[r1], self.roots[r2]
            else:
                a, b = r1, r2

            while self.is_root(a - r * b):
                r += 1
            return r  # in fact, "true r" is (r - 1), but since c_ab = "true r" + 1 we just return r

        res = sign_ab(index1, index2) * val_ab(index1, index2)
        self._N_ab[index1][index2] = res
        return res


@singleton_rs
class Cn(RootSystem):
    def __init__(self, n: int = 3) -> None:
        super().__init__()

        self._name = f"C{n}"
        self._n = n
        self._roots = []

        # Template zeros vector.
        z = Matrix.zeros(1, self._n).tolist()[0]
        # z = zeros(self._n, dtype=int)

        # x_1 ... x_{n-1}: [0, 0, 1, -1, 0, ..., 0]
        for i in range(self._n - 1):
            _ = z.copy()
            _[i], _[i + 1] = 1, -1
            self._roots.append(_)

        # x_n = [0, ..., 0, 2]
        _ = z.copy()
        _[-1] = 2
        self._roots.append(_)

        # [0, 1, 0, ..., 0, -1, 0, ..., 0]    j - i > 1
        for i in range(self._n - 2):
            for j in range(i + 2, self._n):
                _ = z.copy()
                _[i], _[j] = 1, -1
                self._roots.append(_)

        # [0, 2, 0, ..., 0]
        for i in range(self._n - 1):
            _ = z.copy()
            _[i] = 2
            self._roots.append(_)

        # [0, 1, 0, ..., 0, 1, 0, ..., 0]     j - i > 0
        for i in range(self._n - 1):
            for j in range(i + 1, self._n):
                _ = z.copy()
                _[i], _[j] = 1, 1
                self._roots.append(_)

        self._roots = list(map(np.array, self._roots))

        # ALL NEGATIVE
        pos_len = len(self._roots)
        for i in range(pos_len):
            self._roots.append(-self._roots[i])

        self._basis = set([tuple(_) for _ in self._roots[:self._n]])

        self._size = len(self._roots)
        self._root_to_index: Dict[Tuple[int, ...], int] = {}
        for i in range(self._size):
            self._root_to_index[tuple(self._roots[i])] = i

        super()._precompute_structure_constants_of_correspondent_lie_algebra()

    def __repr__(self):
        return f"Cn({self._n})"

    @property
    def n(self) -> int:
        return self._n

    @property
    def name(self) -> str:
        return self._name

    @property
    def size(self) -> int:
        return self._size

    @property
    def basis(self) -> Set[Tuple[int, ...]]:
        return self._basis

    @property
    def roots(self) -> List[array]:
        return self._roots

    @property
    def root_to_index(self) -> Dict[Tuple[int, ...], int]:
        return self._root_to_index

    @property
    def structure_constants(self) -> List[List[Tuple[Tuple[Optional[int], Optional[int]], ...]]]:  # todo: looks weird
        return self._structure_constants


@singleton_rs
class F4(RootSystem):
    def __init__(self) -> None:
        super().__init__()

        self._name = f"F4"
        self._n = 4

        # TODO: this is a quick way of generating F4 roots for my investigation. Must be rewritten in a better way.
        # ---------------------- START OF CODE TO BE REMASTERED ----------------------
        transformation_matrix = [[0, 0, 0, 1 / 2], [1, 0, 0, -1 / 2], [-1, 1, 0, -1 / 2], [0, -1, 1, -1 / 2]]
        transformation_matrix = Matrix([list(map(Rational, x)) for x in transformation_matrix])
        _roots = [[1, 0, 0, 0],
                  [0, 1, 0, 0],
                  [0, 0, 1, 0],
                  [0, 0, 0, 1],
                  [1, 1, 0, 0],
                  [0, 1, 1, 0],
                  [0, 0, 1, 1],
                  [1, 1, 1, 0],
                  [0, 1, 1, 1],
                  [0, 1, 2, 0],
                  [0, 1, 2, 1],
                  [1, 1, 1, 1],
                  [1, 1, 2, 0],
                  [0, 1, 2, 2],
                  [1, 1, 2, 1],
                  [1, 2, 2, 0],
                  [1, 1, 2, 2],
                  [1, 2, 2, 1],
                  [1, 2, 2, 2],
                  [1, 2, 3, 1],
                  [1, 2, 3, 2],
                  [1, 2, 4, 2],
                  [1, 3, 4, 2],
                  [2, 3, 4, 2]]
        for x in _roots.copy():
            _roots.append([-_ for _ in x])
        _roots = Matrix(_roots)
        _roots *= transformation_matrix.T
        self._roots = list(map(np.array, _roots.tolist()))
        # ---------------------- END OF CODE TO BE REMASTERED ----------------------

        self._basis = set([tuple(_) for _ in self._roots[:self._n]])

        self._size = len(self._roots)
        self._root_to_index: Dict[Tuple[int, ...], int] = {}
        for i in range(self._size):
            self._root_to_index[tuple(self._roots[i])] = i

        super()._precompute_structure_constants_of_correspondent_lie_algebra()

    def __repr__(self):
        return f"F4"

    @property
    def n(self) -> int:
        return self._n

    @property
    def name(self) -> str:
        return self._name

    @property
    def size(self) -> int:
        return self._size

    @property
    def basis(self) -> Set[Tuple[int, ...]]:
        return self._basis

    @property
    def roots(self) -> List[array]:
        return self._roots

    @property
    def root_to_index(self) -> Dict[Tuple[int, ...], int]:
        return self._root_to_index

    @property
    def structure_constants(self) -> List[List[Tuple[Tuple[Optional[int], Optional[int]], ...]]]:  # todo: looks weird
        return self._structure_constants


# Well, it's an antipattern for sure :) But I have no willing to make it singleton or smth.
# P.S.: Just be careful with it xD
class DefaultRS:
    rs = Cn(3)

    @classmethod
    def reset_default_root_system(cls, rs: RootSystem):
        cls.rs = rs

# TODO: while implementing F4, I recalled that roots (in R^n) can be represented with non-integer
#  values as well (e.g. 1/2). Since we need precise computations (e.g. cartan product e.t.c.),
#  we can't use float type. C_n and, maybe, RootSystem, some typings, ... must be rewritten s.t.
#  roots are lists of sympy.Rational values.
