from chevallib.algebra import *
from chevallib.group import Group
from chevallib.roots import *
from sympy import Matrix


def ring_commute(a, b):
    a, b = AdjointRepresentation(a).matrix(), AdjointRepresentation(b).matrix()
    return a * b - b * a


def get_res(i, j, a):
    res = None
    for val, idx in a.rs.structure_constants[i][j]:
        d = a.rs.size + a.rs.n
        elem = Matrix.zeros(1, d)

        if idx >= a.rs.size:
            elem = Matrix(a.get_element(f"h{idx + 1 - a.rs.size}"))
        elif idx >= 0:
            elem = Matrix(a.get_element(f"x{idx + 1}"))

        if res is None:
            res = val * elem
        else:
            res += val * elem
    return AdjointRepresentation(AlgebraElement(data=list(res), rs=a.rs)).matrix()


##################################  TESTS  ##################################


def test_group_relations_xx(rs=DefaultRS().rs):
    # Checking [xi, xj]

    a = Algebra(rs)
    for i in range(a.rs.size):
        for j in range(a.rs.size):
            assert ring_commute(a.get_element(f"x{i + 1}"), a.get_element(f"x{j + 1}")) == get_res(i, j, a)


def test_group_relations_xh(rs=DefaultRS().rs):
    # Checking [x, h]; [h, x]

    a = Algebra(rs)
    for i in range(a.rs.size):
        for j in range(a.rs.n):
            assert ring_commute(a.get_element(f"x{i + 1}"), a.get_element(f"h{j + 1}")) == get_res(i, j + a.rs.size, a)
            assert ring_commute(a.get_element(f"h{j + 1}"), a.get_element(f"x{i + 1}")) == get_res(a.rs.size + j, i, a)


def test_group_relations_hh(rs=DefaultRS().rs):
    # Checking [x, h]; [h, x]

    a = Algebra(rs)
    for i in range(a.rs.n):
        for j in range(a.rs.n):
            assert ring_commute(a.get_element(f"h{i + 1}"), a.get_element(f"h{j + 1}")) == \
                   get_res(i + a.rs.size, j + a.rs.size, a)


def test_Q_cubed_Q_commute(rs=DefaultRS().rs):
    # Q**3 == E and Q1 * Q2 = Q2 * Q1. Careful with system type: correct for not all of them

    g = Group(rs)
    assert g.Q1 ** 3 == Matrix.eye(*g.Q1.shape)
    assert g.Q3 ** 3 == Matrix.eye(*g.Q1.shape)
    assert g.Q1 * g.Q3 == g.Q3 * g.Q1


def test_structure_constants_symmetry(rs=DefaultRS().rs):
    for i in range(rs.size + rs.n):
        for j in range(rs.size + rs.n):
            assert len(rs.structure_constants[i][j]) == len(rs.structure_constants[j][i])
            for _ in range(len(rs.structure_constants[i][j])):
                val1, id1 = rs.structure_constants[i][j][_]
                val2, id2 = rs.structure_constants[j][i][_]
                assert val1 == -val2 and id1 == id2
