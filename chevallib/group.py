from sympy import symbols, Matrix
from re import compile

from chevallib.algebra import AlgebraElement, AdjointRepresentation
from chevallib.roots import RootSystem, DefaultRS


# todo: make utils again, move to utils
def custom_exp(M):
    m, res, z = M, Matrix.eye(*M.shape), Matrix.zeros(*M.shape)
    denom, n = 1, 2
    while not m.equals(z):
        res += m / denom
        denom *= n
        n += 1
        m *= M
    return res


class Group:
    def __init__(self, rs: RootSystem = DefaultRS.rs):
        if not isinstance(rs, RootSystem):
            raise TypeError("Root system must be of type RootSystem")
        self.rs = rs
        self.name_pattern = compile(r"(h[1-9]\d*)|(x[1-9]\d*)")

    def __getattr__(self, name: str):
        self.__setattr__(name, self.get_element(name))
        return self.__getattribute__(name)

    def get_element(self, name: str) -> 'GroupElement':
        """
        Returns a group element named `name`. Name may equal 'xi', 'wi', 'hi' or 'Qi'.

        :param name: name of a group element in 1-indexation
        :return: required group element
        """

        # TODO: exactly here - looks like shit :)
        t = symbols('t')

        # TODO: add more complex and correct name checking
        assert int(name[1:]) > 0, f"Number in name {name} must be greater than 0"
        if name[0] == 'w':
            i = int(name[1:])

            # x_{alpha}(t)
            x_a_t = self.get_element(f"x{i}")
            # x_{-alpha}(-t^-1)
            ma = self.rs.get_negative_index(i - 1) + 1
            x_ma_mt = self.get_element(f"x{ma}").subs({'t': -t ** -1})

            return x_a_t * x_ma_mt * x_a_t

        if name[0] == 'Q':
            i = int(name[1:])
            return self.get_element(f"w{i}")(1) * self.get_element(f"x{i}")(1)

        if name[0] == 'h':
            i = int(name[1:])
            return self.get_element(f"w{i}") * self.get_element(f"w{i}")(1) ** -1

        Ad_x = AdjointRepresentation(AlgebraElement(name, rs=self.rs)).matrix()
        g_x = GroupElement(custom_exp(Ad_x * t))
        self.__setattr__(name, g_x)
        return self.__getattribute__(name)


class GroupElement(Matrix):
    def __call__(self, k):
        return self.subs({'t': k})
