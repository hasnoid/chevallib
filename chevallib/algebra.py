from itertools import product
from re import compile

import sympy.core
from sympy import Matrix
from typing import Iterable, Union

from chevallib.roots import DefaultRS, RootSystem


class AlgebraElement(list):
    # TODO: add (bin)pow operation

    def __init__(self, name: str = None, data: Iterable[int] = None,
                 rs: RootSystem = DefaultRS.rs):
        if not ((name is None) ^ (data is None)):
            raise ValueError("You must specify exactly one of: name, data")

        self.rs = rs
        self.n = self.rs.n
        self.dim = self.rs.size + self.n

        if name is not None:
            # name is specified, data must be derived

            # TODO: add correctness checking here!
            self.name = name

            super().__init__([0] * self.dim)
            if name[0] == 'x':
                self[int(name[1:]) - 1] = 1
            elif name[0] == 'h':
                self[self.rs.size + int(name[1:]) - 1] = 1
            else:
                raise NotImplementedError(f"Expected name[0] == 'x' or 'h', got: {name}")

        if data is not None:
            super().__init__(data)
            self.name = ''
            self.update_name()

    def __add__(self, other) -> 'AlgebraElement':
        if not len(self) == len(other):
            raise ValueError("Elements must be of same shapes. "
                             f"Given: {len(self)}, {len(other)}")

        res = [0] * len(self)
        for i in range(len(self)):
            res[i] = self[i] + other[i]

        return AlgebraElement(data=res, rs=self.rs)

    def __sub__(self, other) -> 'AlgebraElement':
        if not len(self) == len(other):
            raise ValueError("Elements must be of same shapes. "
                             f"Given: {len(self)}, {len(other)}")

        res = [0] * len(self)
        for i in range(len(self)):
            res[i] = self[i] - other[i]

        return AlgebraElement(data=res, rs=self.rs)

    def __iadd__(self, other) -> 'AlgebraElement':
        return self + other

    def __mul__(self, other) -> 'AlgebraElement':
        if isinstance(other, int) or isinstance(other, sympy.core.Number):
            res = [0] * len(self)
            for i in range(len(self)):
                res[i] = self[i] * other

            return AlgebraElement(data=res, rs=self.rs)

        if isinstance(other, AlgebraElement):
            if not len(self) == len(other):
                raise ValueError("Elements must be of same shapes. "
                                 f"Given: {len(self)}, {len(other)}")
            return self._commute(other)

        raise NotImplementedError(f"Can't multiply AlgebraElement with {type(other)}")

    def __rmul__(self, other) -> 'AlgebraElement':
        # Todo: remove the following three lines? Seem to be redundant
        m = 1
        if isinstance(other, AlgebraElement):
            m = -1
        return self.__mul__(other) * m

    def _commute(self, other):
        res = AlgebraElement(data=[0] * len(self), rs=self.rs)
        indices1, indices2 = [], []
        for _ in range(len(self)):
            if self[_]:
                indices1.append(_)
            if other[_]:
                indices2.append(_)

        for i, j in product(indices1, indices2):
            res += self[i] * other[j] * self._commute_basic(i, j)

        return res

    # TODO: well, architecturally, it must be a method of the Algebra class
    def _commute_basic(self, i: int, j: int) -> 'AlgebraElement':
        """
        Commute two basic algebraical elements.

        :param i: index of the first basic element
        :param j: index of the second basic element (note: this operation is anti-commutative)
        :return: AlgebraElement = [basic_element_i, basic_element_j]
        """
        res = AlgebraElement(data=[0] * (self.rs.size + self.rs.n), rs=self.rs)
        for coeff, res_ind in self.rs.structure_constants[i][j]:
            res[res_ind] = coeff
        return res

    def update_name(self) -> None:
        _ = []

        for i in range(self.rs.size):
            if self[i]:
                _.append(f"{self[i] if self[i] != 1 else ''}x{i + 1}")

        delta = self.rs.size
        for i in range(self.rs.size, self.rs.size + self.n):
            if self[i]:
                _.append(f"{self[i] if self[i] != 1 else ''}h{i + 1 - delta}")

        if len(_) == 0:
            _ = ['0']

        self.name = ' + '.join(_)

    def __repr__(self):
        self.update_name()
        return self.name

    def __str__(self):
        return super().__repr__()


# TODO: maybe should be reimplemented as singleton (with no RootSystemParams)
# TODO: alternative: store a root system in each AE.
#  (Problem: forbid creating AE? Otherwise - how to define root system? Manually.)
class Algebra:
    def __init__(self, rs: RootSystem = DefaultRS.rs):
        self.rs = rs
        self.n = rs.n
        self.name_pattern = compile(r"(h[1-9]\d*)|(x[1-9]\d*)")

    def __getattr__(self, name: str):
        self.__setattr__(name, self.get_element(name))  # caching
        return self.__getattribute__(name)

    def get_element(self, name: str):
        if not self.name_pattern.match(name) or \
                name[0] == 'h' and not (0 < int(name[1:]) <= self.n) or \
                name[0] == 'x' and not (0 < int(name[1:]) <= self.rs.size):
            # Todo: change the text
            raise AttributeError("Algebra can return basic elements named "
                                 f"h[1-{self.n}] or x[1-{self.rs.size}]. Got query: {name}")

        # We don't initialize them all in advance to save memory and speed up Algebra
        return AlgebraElement(name=name, rs=self.rs)


class AdjointRepresentation:
    def __init__(self, x: AlgebraElement):
        self.x = x
        self._matrix: Union[Matrix, None] = None

    def matrix(self) -> Matrix:
        if self._matrix is None:
            sz = self.x.dim
            pre_matrix = []
            for i in range(sz - self.x.n):
                pre_matrix.append(self.x * AlgebraElement(f"x{i + 1}", rs=self.x.rs))  # todo: err... i -> str -> i
            for i in range(self.x.n):
                pre_matrix.append(self.x * AlgebraElement(f"h{i + 1}", rs=self.x.rs))
            self._matrix = Matrix(pre_matrix).T

        return self._matrix

    def __call__(self, y: AlgebraElement) -> AlgebraElement:
        return self.x * y
