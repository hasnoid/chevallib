# General

Chevallib is a simple package that provides the interfaces for:
- `roots` - root systems - including discovering the structure, roots operations
- `algebra` - semi-simple Lie algebras - including Chevalley basis, **its structure constants**, algebraic operations, elements representations
- `group` - groups of Lie type (Chevalley groups) - including group operations, elements explicit representation as matrices.

# Code snippets

### Roots system
```python
pass  # TBD
```

### Lie algebra
```python
pass  # TBD
```

### Chevalley group
```python
pass  # TBD
```

### Correctness proof
```python
pass  # TBD
```

### Import everything
Mostly, for debugging the code. Highly likely, you may not want to use this code snippet :)
```python
from chevallib.algebra import *
from chevallib.group import *
from chevallib.roots import *
from chevallib.test_group import *

# Pretty matrix output
from sympy.interactive.printing import init_printing
init_printing(use_unicode=True, wrap_line=False)

# Relations code: not used in main branch
# from relations.C3 import *
```

# TODO (19.04.22)
- #### Linearisation++
  - [x] Construct all relations
  - [x] Linearise all relations (+ mod reduction)
  - [ ] Check whether the solution is a single one
- #### Testing:
  - [x] Write unit tests
  - [ ] Manage CI/CD
- #### Checking relations (maybe related to testing):
  - [x] Add checking all basic relations
- #### Profiling:
  - [x] Now some pieces of code run extremely time. E.g. the following code:
    ```python
    from chevallib.group import Group
    from chevallib.roots import *
    g = Group(F4())
    Q1, Q3 = g.Q1, g.Q3  # this part in fact
    ```
- #### Root systems:
  - [ ] Add the support all r.s., e.g. by inheriting from sympy root systems
  - [x] Move structure constants output to a separate function
  - [ ] Move ordering to a separate function
  - [x] Add caching of N_ab: now it doesn't remember the counted values and each time goes deep in recursion until reaching an extraspecial pair
    - [x] Control the correct order of counting N_ab: described in the paper regarding the signs
- #### Docstrings:
  - [ ] roots + ...
  - [ ] group
  - [ ] algebra + algebra element
- #### Readme:
  - [ ] General project description
  - [ ] Code snippets for particular tasks and most common usecases
  - [ ] Tags or smth
  - [ ] Badges :D
- #### Misc:
  - [ ] Group element doesn't support Matrix methods. Fix.

# TODO (march 22)

- Highest priority:
  - [x] adequate readme, not just a todo list :-)
  - [x] add algebra support
  - [x] add group support
  - [x] add pytest or another testing system to permanently control the code correctness 
- General:
  - [x] revise the structure of the project
- `roots.py` 
  - [x] root systems turn to be already implemented in sympy.liealgebras.type_*.
    - Think over whether there is anything to be added (e.g. root decomposition or series) 
    - Add the support of this type to the project
    
    <span style="color:gray">**Conclusion**: now I've decided to leave untouched the structure of the project since the `AlgebraElement`, `Group` and the general logic deeply utilize the particular structure of a root system: <br>**(pos_roots, other, neg_roots, neg_other)** <br>ordered by height inside each of that subgroups. I don't know whether the ordering in the sympy package is the same.</span>
  - [x] add decomposition function, e.g. *rs.decompose(vec) -> required decomposition by basic roots*. <br>Idk whether I do need it :)
  - [x] move supported_system_types and C_n logic to a separate subclass or smth like that. E.g, rs is a generic class, specific systems are inherited from it.
  - [x] use faster structures (e.g. sets in *is_...* methods to achieve O(1) or at least O(log) time). Google or explore & write a post on this topic. <br>**Note:** will it really be worth it since the size of structures is typically no more than 100? Compare, take some overhead into account.

    <span style="color:gray">**Regarding the Note**: yes, since we are to ask smth like "is this vector a root" rather often, so it's more than justified to be able to do it instantly.</span>
- `Utils.py`
  - [x] *todo*: check pattern matching more carefully, e.g. F1 or E1 pass the test albeit these root systems are incorrect.